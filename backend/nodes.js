const node0 = {
    id: 0,
    img: 'start',
    text: 'Дайте имя юному волшебнику.'
}

const node1 = {
    id: 1,
    img: 'start',
    text: 'Какую магию мы будем учить?',
    options: [
        { text: 'Светлую', id: 2 },
        { text: 'Тёмную', id: 3 }
    ]
}

const node2 = {
    id: 2,
    img: 'light mage',
    text: 'Выбери заклинание, которое ты будешь учить.',
    options: [
        { text: 'Превращение человека в бабочку.', id: 4 },
        { text: 'Превращение человека в кошку.', id: 5 }
    ]
}

const node3 = {
    id: 3,
    img: 'darkmage',
    text: 'Выбери заклинание, которое ты будешь учить.',
    options: [
        { text: 'Превращение человека в муху.', id: 6 },
        { text: 'Превращение человека в змею.', id: 7 }
    ]
}

const node4 = {
    id: 4,
    img: 'butterfly',
    text: 'Выбери способность:',
    options: [
        { text: 'Летать', id: 8 },
        { text: 'Иллюзии', id: 9 },
        { text: 'Зелья', id: 10 }
    ]
}

const node5 = {
    id: 5,
    img: 'cat',
    text: 'Выбери способность:',
    options: [
        { text: 'Летать', id: 8 },
        { text: 'Иллюзии', id: 9 },
        { text: 'Зелья', id: 10 }
    ]
}

const node6 = {
    id: 6,
    img: 'fly',
    text: 'Выбери способность:',
    options: [
        { text: 'Летать', id: 8 },
        { text: 'Иллюзии', id: 9 },
        { text: 'Зелья', id: 10 }
    ]
}

const node7 = {
    id: 7,
    img: 'snake',
    text: 'Выбери способность:',
    options: [
        { text: 'Летать', id: 8 },
        { text: 'Иллюзии', id: 9 },
        { text: 'Зелья', id: 10 }
    ]
}

const node8 = {
    id: 8,
    img: 'wings',
    text: 'Выбери талисман:',
    options: [
        { text: 'Плащ', id: 11 },
        { text: 'Кольцо', id: 12 }
    ]
}

const node9 = {
    id: 9,
    img: 'copies',
    text: 'Выбери талисман:',
    options: [
        { text: 'Кольцо', id: 12 },
        { text: 'Перчатки', id: 13 }
    ]
}

/*poison_on_the_ground было бы тоже неплохо вставить.*/
const node10 = {
    id: 10,
    img: 'poison_with_hand',
    text: 'Выбери талисман:',
    options: [
        { text: 'Перчатки', id: 13 },
        { text: 'Кинжал', id: 14 }
    ]
}

const node11 = {
    id: 11,
    img: 'cloak',
    text: 'Какую силу ты хочешь хранить?',
    options: [
        { text: 'Пространство', id: 15 },
        { text: 'Время', id: 16 }
    ]
}

const node12 = {
    id: 12,
    img: 'ring',
    text: 'Какую силу ты хочешь хранить?',
    options: [
        { text: 'Разум', id: 19 },
        { text: 'Душа', id: 20 }
    ]
}

const node13 = {
    id: 13,
    img: 'gloves',
    text: 'Какую силу ты хочешь хранить?',
    options: [
        { text: 'Ледяной кинжал', id: 21 },
        { text: 'Вода', id: 22 }
    ]
}

const node14 = {
    id: 14,
    img: 'knife',
    text: 'Какую силу ты хочешь хранить?',
    options: [
        { text: 'Темная энергия', id: 17 },
        { text: 'Загробный мир', id: 18 }
    ]
}

const node15 = {
    id: 15,
    img: 'space',
    options: []
}

const node16 = {
    id: 16,
    img: 'time',
    options: []
}

const node17 = {
    id: 17,
    img: 'dark_energy',
    options: []
}

const node18 = {
    id: 18,
    img: 'death_world',
    options: []
}

const node19 = {
    id: 19,
    img: 'mind',
    options: []
}

const node20 = {
    id: 20,
    img: 'soul',
    options: []
}
const node21 = {
    id: 21,
    img: 'frozen_knife',
    options: []
}

const node22 = {
    id: 22,
    img: 'water',
    options: []
}

const mapIdToMap = {
    0: node0,
    1: node1,
    2: node2,
    3: node3,
    4: node4,
    5: node5,
    6: node6,
    7: node7,
    8: node8,
    9: node9,
    10: node10,
    11: node11,
    12: node12,
    13: node13,
    14: node14,
    15: node15,
    16: node16,
    17: node17,
    18: node18,
    19: node19,
    20: node20,
    21: node21,
    22: node22
  }

module.exports = mapIdToMap;

/*
====================================================
const node0 = {
    id: 0,
    img: 'https://mocah.org/uploads/posts/1143495-animals-wooden-surface-dog-puppies-Nova-Scotia-Duck-Tolling-Retriever-puppy-mammal-vertebrate-dog-like-mammal-golden-retriever-dog-crossbreeds-retriever.jpg',
    text: 'It\'s sooo cute puppyeeee',
    options: [
        { text: 'Maybe kitty?', id: 1 },
        { text: 'Maybe hamster?', id: 2 }
    ]
}

const node1 = {
    id: 1,
    img: 'https://wallup.net/wp-content/uploads/2016/01/184795-animals-kittens-cat.jpg',
    text: 'So сute!',
    options: [
        { text: 'Maybe doggy?', id: 0 },
        { text: 'Maybe hamster?', id: 2 }
    ]
}

const node2 = {
    id: 2,
    img: 'https://www.aysetolga.com/wp-content/uploads/2017/05/hamster-3.jpg',
    text: 'He loves nuts!!!',
    options: [
        { text: 'Maybe doggy?', id: 0 },
        { text: 'Maybe kitty?', id: 1 }
    ]
}
====================================================
*/