import { Container } from '@sberdevices/plasma-ui/components/Grid';
import './App.css';
import Scene from './components/scene.js';

function App() {
  return (
    <Container>
      <Scene />
    </Container>
  );
}

export default App;
