import getScene from '../APIHelper.js'
import React, { useEffect, useState } from 'react';

import { darkSber } from '@sberdevices/plasma-tokens/themes';
import { Button } from '@sberdevices/ui/components/Button/Button';
import { Container } from '@sberdevices/plasma-ui/components/Grid';
import { Image } from '@sberdevices/ui/components/Image/Image'

import { API_URL } from '../APIHelper.js';

let currentId = 1;

const styles = {
    marginLeft: '20px',
    color: 'red'
  }

function Scene() {
    const [scene, setScene] = useState(null); 
    
    const fetchedData = async (id) => {
        return await getScene(id);
    }
    
    useEffect(() => {
        fetchedData(currentId).then((response) => {
            console.log(response);
            const { data } = response;
            setScene(data);
        })
    }, []);

    const moveTo = (nextId) => () =>  {
        fetchedData(nextId).then((response) => {
            const { data } = response;
            setScene(data);
        })
    }    


    if (scene) {
        if (scene.options) {
            return(
                <Container styles={darkSber} >
                    <Container>
                        <img src={API_URL + '/' + scene.img + '.png' } height={'450'} width={'450'} />
                        <h1> { scene.text } </h1>
                    </Container> 
                    <Container>
                    {
                        scene.options.map((item) => { 
                            return (
                                <Button  size="l" onClick={moveTo(item.id)}>
                                    {item.text}
                                </Button>
                            );
                        })
                    }
                    </Container>
                </Container>
            );
        } else {
            return(
                <Container styles={darkSber} >
                    <img src={API_URL + '/' + scene.img + '.png' } height={'450'} width={'450'} />
                    <h1> { scene.text } </h1>
                </Container>
            )
        }
        
    } else {
        return <h1>Nothing...</h1>
    } 
    
}

export default Scene;